import React, { useState, useEffect } from "react";
import * as d3 from "d3";

import TopBar from "./components/TopBar/TopBar";
import Filter from "./components/Filter/Filter";
import LineChart from "./components/LineChart/LineChart";
import BarChart from "./components/BarChart/BarChart";
import RechartsExample from "./components/RechartsExample/RechartsExample";

const app = (props) => {
  const [data, setData] = useState({});
  const [sliderValues, setSliderValuesHandler] = useState([]);
  const [coin, setCoinHandler] = useState('');
  const [yValue, setYValueHandler] = useState('');

  const parseTime = d3.timeParse("%d/%m/%Y");
  const formatTime = d3.timeFormat("%d/%m/%Y");

  function prepareData(rawData) {
    let preparedData = {};
    for (var coin in rawData) {
      if (!rawData.hasOwnProperty(coin)) {
          continue;
      }
      preparedData[coin] = rawData[coin].filter(d => d["price_usd"] != null);
      preparedData[coin].forEach(d => {
        d["price_usd"] = +d["price_usd"];
        d["24h_vol"] = +d["24h_vol"];
        d["market_cap"] = +d["market_cap"];
        d["date"] = parseTime(d["date"]);
      });
    }

    return preparedData;
  };

  function formatDateToMiliseconds(data) {
    for (var coin in data) {
      data[coin].forEach(d => {
        if (d["date"] instanceof Date) {
          d["date"] = d["date"].getTime();
        }
      });
    }
    return data;
  }

  useEffect(() => {
    d3.json("./data/coins.json").then(rawData => {
      setData(prepareData(rawData));
    });
  }, []);

  return (
    <div className="App">
      <TopBar />
      
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <Filter 
              setSliderValues={setSliderValuesHandler} 
              setCoin={setCoinHandler} 
              setYValue={setYValueHandler}
              parseTime={parseTime}
              formatTime={formatTime} />
          </div>
        </div>

        <div className="row">
          <div className="col-md-12">
            <LineChart 
              data={data} 
              sliderValues={sliderValues} 
              coin={coin} 
              yValue={yValue} />
          </div>
        </div>

        <hr /> 

        <div className="row">
          <div className="col-md-12">
            <h1>Recharts</h1>
            <RechartsExample
              data={formatDateToMiliseconds(data)} 
              sliderValues={sliderValues} 
              coin={coin}
              yValue={yValue} />
          </div>
        </div>

        <hr /> 

        <div className="row">
          <div className="col-md-12">
            <h1>Data updates with node manipulation</h1>
            <BarChart />
          </div>
        </div>

      </div>
    </div>
  );
}

export default app;
