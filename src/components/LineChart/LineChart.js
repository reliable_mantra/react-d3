import React, { useEffect } from "react";
import * as d3 from "d3";

const lineChart = (props) => {
  let g = null;
  
  // 1. Setup SVG
  const margin = { left:80, right:100, top:50, bottom:100 },
        height = 500 - margin.top - margin.bottom, 
        width = 800 - margin.left - margin.right;

  function update() {
    if (Object.entries(props.data).length === 0) {
      return;
    }

    // Filter data based on selections
    let filteredData = props.data[props.coin].filter(d => {
      return ((d.date >= props.sliderValues[0]) && (d.date <= props.sliderValues[1]))
    });

    // 2. Transition
    const t = () => d3.transition().duration(1000);    

    // 3. Scales
    const x = d3.scaleTime()
      .domain(d3.extent(filteredData, d => d.date))
      .range([0, width]);
    const y = d3.scaleLinear()
      .domain([
        d3.min(filteredData, d => d[props.yValue]), //  / 1.005
        d3.max(filteredData, d => d[props.yValue])  //  * 1.005
      ])
      .range([height, 0]);

    // 4. Axes
    const xAxisCall = d3.axisBottom()
      .ticks(4)
      .scale(x);
    d3.select("#chart-area > svg g.x.axis")
      .transition(t())
      .call(xAxisCall);
    const yAxisCall = d3.axisLeft()
      .scale(y);
    d3.select("#chart-area > svg g.y.axis")
      .transition(t()).call(yAxisCall.tickFormat(y => {
        // Fix for format values
        let formattedValue = d3.format(".2s")(y);
        switch (formattedValue.slice(-1)) {
            case "G": return formattedValue.slice(0, -1) + "B";
            case "k": return formattedValue.slice(0, -1) + "K";
        }
        return formattedValue;
      }));

    // 5. Path generator
    const line = d3.line()
      .x(d => x(d.date))
      .y(d => y(d[props.yValue]));

    // 6. Update our line path
    // const g = d3.select("#chart-area > svg > g");
    g = d3.select(g);
    g.select(".line")
      .transition(t)
      .attr("d", line(filteredData));

    // Update y-axis labels
    const newText = (props.yValue === "price_usd") ? "Price (USD)" :
      ((props.yValue === "market_cap") ?  "Market Capitalization (USD)" : "24 Hour Trading Volume (USD)");
      d3.select("#chart-area > svg text.y.axisLabel").text(newText);
  }

  // Set the update hook
  useEffect(() => {
    update();
  }, [props.data, props.sliderValues, props.coin, props.yValue]);

  return (
    <div id="chart-area">
      <svg width={width + margin.left + margin.right} height={height + margin.top + margin.bottom}>
        <g transform={`translate(${margin.left}, ${margin.top})`} ref={(el) => { g = el; }}>
          <path className="line" fill="none" stroke="grey" strokeWidth="3px" />
          <g className="x axis" transform={`translate(0, ${height})`} />
          <g className="y axis" />
          <text className="x axisLabel" x={width/2} y={height+50} fontSize="20px" textAnchor="middle">Time</text>
          <text className="y axisLabel" transform="rotate(-90)" x="-170" y="-60" fontSize="20px" textAnchor="middle">Price (USD)</text>
        </g>
      </svg>
    </div>
  );
};

export default lineChart;