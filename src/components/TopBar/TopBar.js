import React from "react";

const topBar = (props) => {
  return (
    <nav className="navbar navbar-default">
      <div className="container">
        <div className="navbar-header">
            <a href="#" className="navbar-brand">CoinStats</a>
        </div>
      </div>
    </nav>
  );
};

export default topBar;