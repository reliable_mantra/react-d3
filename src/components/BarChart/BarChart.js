import React, { useState, useEffect } from "react";
import * as d3 from "d3";

const dataPool = [
  [100, 250, 175, 200, 120],
  [230, 120, 300, 145, 75, 250],
  [240, 250, 100]
];

const barChart = (props) => {
  let svg = null;

  const [index, setIndex] = useState(0);

  const rectWidth = 100;
  const height = 300;
  const width = 600;
  
  const colors = d3.scaleOrdinal(d3.schemeCategory10);
  const t = d3.transition().duration(1000);

  function update(data) {
    svg = d3.select(svg);
    var bars = svg.selectAll('rect')
      .data(data, d => d);

    // exit
    bars.exit()
      .transition(t)
      .attr('y', height)
      .attr('height', 0)
      .remove();

    // enter
    var enter = bars.enter().append('rect')
      .attr('width', rectWidth)
      .attr('stroke', '#fff')
      .attr('y', height);

    // enter + update
    bars = enter.merge(bars)
      .attr('x', (d, i) => i * rectWidth)
      .attr('fill', d => colors(d))
      .transition(t)
      .attr('y', d => height - d)
      .attr('height', d => d);
  }

  useEffect(() => {
    update(dataPool[index % 3]);
  }, [index]);

  return (
    <div>
      <button onClick={() => setIndex(index + 1)}>Update</button>
      <p>{index}</p>
      <svg 
        width={width} 
        height={height}
        ref={(el) => { svg = el; }}></svg>
    </div>
  );
};

export default barChart;