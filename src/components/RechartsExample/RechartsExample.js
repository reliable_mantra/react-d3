import React from "react";
import { LineChart, Line, XAxis, YAxis} from "recharts";
import * as d3 from "d3";
import moment from "moment";
import "./RechartsExample.css";

const rechartsExample = (props) => {
  let filteredData = [];
  const margin = { top: 20, right: 20, bottom: 20, left: 20 },
        height = 500 - margin.top - margin.bottom, 
        width = 800 - margin.left - margin.right;

  if (Object.entries(props.data).length !== 0) {
    filteredData = props.data[props.coin].filter(d => {
      return ((d.date >= props.sliderValues[0]) && (d.date <= props.sliderValues[1]))
    });
  }

  // Update y-axis labels
  const yAxisLabel = (props.yValue === "price_usd") ? "Price (USD)" :
    ((props.yValue === "market_cap") ?  "Market Capitalization (USD)" : "24 Hour Trading Volume (USD)");
  d3.select(".rechart text.y.axisLabel").text(yAxisLabel);

  function yAxisTickFormat(y) { 
    let formattedValue = d3.format(".2s")(y);
    switch (formattedValue.slice(-1)) {
        case "G": return formattedValue.slice(0, -1) + "B";
        case "k": return formattedValue.slice(0, -1) + "K";
    }
    return formattedValue;
  }

  return (
    <LineChart className="rechart" width={width} height={height} data={filteredData} margin={margin}>
      <Line className="line" type="monotone" dataKey={props.yValue} stroke="grey" dot={false} animationDuration={150} />
      <XAxis className="x axis" dataKey="date" scale="time" type="number" domain={['dataMin', 'dataMax']} tickFormatter={(tick) => moment(tick).format('Y')} interval={350} tickCount={4} />
      <YAxis className="y axis" dataKey={props.yValue} scale="linear" tickFormatter={(tick) => yAxisTickFormat(tick)} interval="preserveStartEnd" />
      <text className="x axisLabel" x={width/2 + 40} y={height} fontSize="20px" textAnchor="middle">Time</text>
      <text className="y axisLabel" transform="rotate(-90)" x="-200" y="30" fontSize="20px" textAnchor="middle">Price (USD)</text>
    </LineChart>
  );
};

export default rechartsExample;