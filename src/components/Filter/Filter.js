import React, { useEffect } from "react";
import $ from "jquery";
import 'jquery-ui/ui/widgets/slider';

const filter = {
  sliderValues: {
    min: "12/05/2013",
    max: "31/10/2017"
  },
  coin: "bitcoin",
  yValue: "price_usd"
};

const dataFilter = (props) => {
  useEffect(() => {
    $("#date-slider").slider({
      range: true,
      min: props.parseTime(filter.sliderValues.min).getTime(),
      max: props.parseTime(filter.sliderValues.max).getTime(),
      step: 86400000, // One day
      values: [
        props.parseTime(filter.sliderValues.min).getTime(), 
        props.parseTime(filter.sliderValues.max).getTime()
      ],
      slide: (event, ui) => {
        document.querySelector("#dateLabel1").textContent = props.formatTime(new Date(ui.values[0]));
        document.querySelector("#dateLabel2").textContent = props.formatTime(new Date(ui.values[1]));
        props.setSliderValues(ui.values);
      }
    });

    props.setSliderValues($("#date-slider").slider("values"));
    props.setCoin(filter.coin);
    props.setYValue(filter.yValue);

    return () => {
      console.log("Cleaned up");
      $("#date-slider").slider("destroy");
    };
  }, []);

  return (
    <div id="selections">

      {/* Date Range */}
      <div className="col-md-4">
        <div id="slider-div">
          <label>Date: <span id="dateLabel1">{filter.sliderValues.min}</span> - <span id="dateLabel2">{filter.sliderValues.max}</span></label>
          <div id="date-slider"></div>
        </div>
      </div>

      {/* Select Coin */}
      <div className="col-md-4">
        <select 
          id="coin-select" 
          className="form-control" 
          defaultValue={filter.coin}
          onChange={e => props.setCoin(e.target.value)}>
            <option value="bitcoin">Bitcoin</option>
            <option value="ethereum">Ethereum</option>
            <option value="bitcoin_cash">Bitcoin Cash</option>
            <option value="litecoin">Litecoind</option>
            <option value="ripple">Ripple</option>
        </select>
      </div>

      {/* Select Graph Data */} 
      <div className="col-md-4">
        <select 
          id="var-select" 
          className="form-control" 
          defaultValue={filter.yValue}
          onChange={e => props.setYValue(e.target.value)}>
            <option value="price_usd">Price in dollars</option>
            <option value="market_cap">Market capitalization</option>
            <option value="24h_vol">24 hour trading volume</option>
        </select>
      </div>

    </div>
  );
};

export default dataFilter;